package main

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/kylehqcom/render/html/http/response"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
	"gitlab.com/kylehqcom/stencil/matcher"
)

func main() {
	l := loader.NewFilepathLoader([]string{"../_templates/*.html"}, loader.WithMustParse(true))
	l.RegisterFuncs(template.FuncMap{
		"render": func() string {
			return "Content from render func"
		},
		"flashIsContentTypeHTML": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeHTML
		},
		"flashIsContentTypeJS": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeJS
		},
		"flashIsContentTypeString": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeString
		},
	})

	rr := response.NewRenderResponse()
	rr.UseExecutor(executor.NewTemplateExecutor())
	rr.UseLoader(l)
	rr.UseMatcher(matcher.NewFilepathMatcher(matcher.WithPathRoot("../_templates")))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		r = response.AddFlashAlert(r, `Flash alert <a href="#">Example html escaping using the template/html package</a>`, nil,
			response.WithFlashMessageAsHTML(),
		)
		r = response.AddFlashError(r, "Flash error", nil)
		r = response.AddFlashInfo(r, "Flash info", nil)
		r = response.AddFlashSuccess(r, "Flash success", nil)
		w.Header().Set("Content-Type", "text/html")
		rr.Render(w, r, "layout.html", nil)
	})
	log.Println("Enjoy using Render")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}
