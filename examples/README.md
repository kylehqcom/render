# render-examples

Examples to create your own loaders and matchers to suit your project needs.

### Examples

- [Everyday Example](everyday)
- [Custom Matcher](custom_matcher)
- [Flash Messages](with_flashes)
- [Locale Fallbacks](locale_fallback)

These examples uses the bundled _"RenderResponse"_ to render your requests. This (in my opinion) will cover 95% of your use cases. Note that in order to render the index handler example, we assume that you have a folder structure of

```
your/dir/to/templates/
  error.html
  layout.html
  en/
    index.html
  es/
    index.html
```
