package main

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/kylehqcom/render/html/http/response"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
	"gitlab.com/kylehqcom/stencil/matcher"
)

var l = loader.NewFilepathLoader([]string{"../_templates/*.html", "../_templates/*/*.html"}, loader.WithMustParse(true))

func main() {
	// This example has a yet to be loaded template with a template.func of {{render}}, so we placeholder to ensure parsing
	l.RegisterFuncs(template.FuncMap{
		"render": func() string {
			return "Render called unexpectedly"
		},
		"flashIsContentTypeHTML": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeHTML
		},
		"flashIsContentTypeJS": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeJS
		},
		"flashIsContentTypeString": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeString
		},
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Note we only refer to the filename to lookup as the PathRoot option is defined in setup
		r = response.AddFlashInfo(r, "route is index", nil)
		render(w, r, "index.html", nil)
	})
	log.Println("Enjoy using Render")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}

// The placeholder render method from above will get replaced with this render method on template execution
func render(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}, opts ...response.RenderOption) {
	// Pass a query param eg `?locale=es` to render the es template
	vals := r.URL.Query()
	loc := vals.Get("locale")
	if loc != "es" {
		loc = "en"
	}

	rr := response.NewRenderResponse(
		response.RenderWithBaseTemplate("layout.html"),
		response.RenderWithErrorTemplate("error.html"),
		response.RenderWithExecutor(executor.NewTemplateExecutor()),
		response.RenderWithLoader(l),
	)
	rr.UseMatcher(
		matcher.NewFilepathMatcher(
			matcher.WithPathRoot("../_templates"),
			matcher.WithFallbackLocale("en"),
			matcher.WithLocale(loc),
		))

	// Note that we created the Renderer "withOptions" but this render method receivers "opts" will take precedence on the Render() call.
	// That way you can have default options defined, but call with different options on render as you require.
	rr.Render(w, r, name, data, opts...)
}
