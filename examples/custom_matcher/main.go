package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"gitlab.com/kylehqcom/render/html/http/response"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
)

type maintMatcher struct{}

func (m maintMatcher) Match(c *loader.Collection, pattern string) (*template.Template, error) {
	t := template.New("")
	return template.Must(t.Parse(fmt.Sprintf("Custom matcher that returns the maintenance template on every match. Pattern: %s", pattern))), nil
}

func main() {
	l := loader.NewFilepathLoader([]string{"../_templates/*.html"}, loader.WithMustParse(true))
	l.RegisterFuncs(template.FuncMap{
		"render": func() string {
			return "Content from render func"
		},
		"flashIsContentTypeHTML": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeHTML
		},
		"flashIsContentTypeJS": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeJS
		},
		"flashIsContentTypeString": func(fct response.FlashContentType) bool {
			return fct == response.FlashContentTypeString
		},
	})

	rr := response.NewRenderResponse()
	rr.UseExecutor(executor.NewTemplateExecutor())
	rr.UseLoader(l)
	rr.UseMatcher(maintMatcher{})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Using time to prove that any template name will render the maint matcher template.
		rr.Render(w, r, time.Now().String(), nil)
	})
	log.Println("Enjoy using Render")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}
