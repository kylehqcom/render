# Custom Matcher Stencil Example

Once you have a collection of templates, you can have multiple matchers to determine with template should be rendered for your user.

In this example, we are using a maintenance matcher which enforces all Match calls to always return the "maintenance mode" template to render to the user.
