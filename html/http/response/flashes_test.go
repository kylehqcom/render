package response_test

import (
	"fmt"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/kylehqcom/render/html/http/response"
)

var (
	ft        = response.FlashType("test")
	opt1Key   = "key"
	opt1Value = "value"
	opt2Key   = "key2"
	opt2Value = true
	msg       = "is a test message"
	opts      = map[string]interface{}{opt1Key: opt1Value, opt2Key: opt2Value}
)

func newTestFlash(flashOpts ...response.FlashOption) *response.Flash {
	return response.NewFlash(ft, msg, opts, flashOpts...)
}

func TestNewFlash(t *testing.T) {
	t.Parallel()
	f := newTestFlash()
	if fmt.Sprintf("%T", f) != "*response.Flash" {
		t.Error("Expected a Flash pointer instance")
	}

	if f.Message != msg {
		t.Error("Expected flash message changed?")
	}

	if fmt.Sprintf("%T", f) != "*response.Flash" {
		t.Error("Expected a Flash pointer instance")
	}

	if fmt.Sprintf("%T", f.Opts) != "map[string]interface {}" {
		t.Error("Expected a map[string]interface {}")
	}

	if 0 == f.CreatedAt {
		t.Error("Expected a CreatedAt UnixNano int")
	}

	iVal1, ok := f.Opts[opt1Key]
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts key of %s", opt1Key))
	}

	val1, ok := iVal1.(string)
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts value type of %T", opt1Value))
	}

	if val1 != opt1Value {
		t.Error(fmt.Sprintf("Expected an opts value of %s", opt1Value))
	}

	iVal2, ok := f.Opts[opt2Key]
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts key of %s", opt2Key))
	}

	val2, ok := iVal2.(bool)
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts value type of %T", opt2Value))
	}

	if val2 != opt2Value {
		t.Error(fmt.Sprintf("Expected an opts value of %v", opt2Value))
	}
}

func TestAddFlash(t *testing.T) {
	t.Parallel()
	r := httptest.NewRequest("GET", "http://localhost", nil)
	if response.HasFlashes(r) {
		t.Error("Assert no flash messages on a new request")
	}

	// Adding to request context
	fts := []response.FlashType{response.FlashAlert, response.FlashError, response.FlashInfo, response.FlashSuccess}
	for _, ft := range fts {
		r = response.AddFlash(r, ft, msg, opts)
	}

	if !response.HasFlashes(r) {
		t.Error("Assert Request has flash messages")
	}

	flashes := response.GetFlashes(r)
	if len(flashes) != len(fts) {
		t.Error("Assert number of flash messages matches number of flash types")
	}

	ftsAssert := fts
	var match bool
	for _, ft := range ftsAssert {
		match = false
		for _, f := range flashes {
			if ft == f.FlashType {
				match = true
				break
			}
		}

		// Check that we have had all the flash types assigned
		if !match {
			t.Error("Assert matching of all flash types")
		}
	}
}

func TestAddFlashAlert(t *testing.T) {
	t.Parallel()
	r := httptest.NewRequest("GET", "http://localhost", nil)
	r = response.AddFlashAlert(r, msg, opts)
	if !response.HasFlashes(r) {
		t.Error("Assert has flash message")
	}

	f := response.GetFlashes(r).First()
	if f.FlashType != response.FlashAlert {
		t.Error("Assert has flash type match")
	}
}

func TestAddFlashError(t *testing.T) {
	t.Parallel()
	r := httptest.NewRequest("GET", "http://localhost", nil)
	r = response.AddFlashError(r, msg, opts)
	if !response.HasFlashes(r) {
		t.Error("Assert has flash message")
	}

	f := response.GetFlashes(r).First()
	if f.FlashType != response.FlashError {
		t.Error("Assert has flash type match")
	}
}

func TestAddFlashInfo(t *testing.T) {
	t.Parallel()
	r := httptest.NewRequest("GET", "http://localhost", nil)
	r = response.AddFlashInfo(r, msg, opts)
	if !response.HasFlashes(r) {
		t.Error("Assert has flash message")
	}

	f := response.GetFlashes(r).First()
	if f.FlashType != response.FlashInfo {
		t.Error("Assert has flash type match")
	}
}

func TestAddFlashSuccess(t *testing.T) {
	t.Parallel()
	r := httptest.NewRequest("GET", "http://localhost", nil)
	r = response.AddFlashSuccess(r, msg, opts)
	if !response.HasFlashes(r) {
		t.Error("Assert has flash message")
	}

	f := response.GetFlashes(r).First()
	if f.FlashType != response.FlashSuccess {
		t.Error("Assert has flash type match")
	}
}

func TestBindToRequestQuery(t *testing.T) {
	t.Parallel()
	flashOrigin := newTestFlash()
	flashes := response.Flashes{flashOrigin}
	r := httptest.NewRequest("GET", "http://localhost", nil)

	rawQuery := flashes.BindToRequestQuery(r)
	if !strings.HasPrefix(rawQuery, response.FlashQueryParam+"=") {
		t.Error("Expected raw query prefix does not match stencil default")
	}

	vals := r.URL.Query()
	flashQueryParam := vals.Get(response.FlashQueryParam)
	if flashQueryParam == "" {
		t.Error("Expected a non empty query param value")
	}

	flashes = response.GetFlashes(r)
	if len(flashes) != 1 {
		t.Error("Assert one flash messages")
	}

	f := flashes[0]
	if flashOrigin.CreatedAt != f.CreatedAt {
		t.Error("Expected origin flash CreatedAt to match request flash")
	}

	if flashOrigin.FlashType != f.FlashType {
		t.Error("Expected origin flash Type to match request flash")
	}

	if flashOrigin.Message != f.Message {
		t.Error("Expected origin flash Message to match request flash")
	}

	if f.Opts == nil {
		t.Error("Expected non nil flash Opts on request flash")
	}

	iVal1, ok := f.Opts[opt1Key]
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts key of %s", opt1Key))
	}

	val1, ok := iVal1.(string)
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts value type of %T", opt1Value))
	}

	if val1 != opt1Value {
		t.Error(fmt.Sprintf("Expected an opts value of %s", opt1Value))
	}

	iVal2, ok := f.Opts[opt2Key]
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts key of %s", opt2Key))
	}

	val2, ok := iVal2.(bool)
	if !ok {
		t.Error(fmt.Sprintf("Expected an opts value type of %T", opt2Value))
	}

	if val2 != opt2Value {
		t.Error(fmt.Sprintf("Expected an opts value of %v", opt2Value))
	}
}

func TestFlashesFirst(t *testing.T) {
	t.Parallel()
	flashes := response.Flashes{}
	if flashes.First() != nil {
		t.Error("Expected nil on first")
	}
	f := response.NewFlash(ft, msg, opts)
	flashes = append(flashes, f)
	if f != flashes.First() {
		t.Error("Expected first flash to be equal")
	}
}

func TestFlashesMarshalAndEncode(t *testing.T) {
	t.Parallel()
	flashes := response.Flashes{}
	f := response.NewFlash(response.FlashType("break"), msg, map[string]interface{}{"broken": func() {}})
	flashes = append(flashes, f)
	if flashes.MarshalAndEncode() != "" {
		t.Error("Expected empty return string due to marshal error")
	}
}

func TestFlashMessageAsHTML(t *testing.T) {
	t.Parallel()
	f := newTestFlash(response.WithFlashMessageAsHTML())
	if f.Message != "" {
		t.Error("Expected Message to be an empty string")
	}

	if f.MessageJS != "" {
		t.Error("Expected MessageJS to be an empty string")
	}

	if fmt.Sprintf("%T", f.MessageHTML) != "template.HTML" {
		t.Error("Expected message to be of type template.HTML")
	}

	if f.MessageHTML != "is a test message" {
		t.Error("Incorrect expected message")
	}
}

func TestWithFlashMessageAsJS(t *testing.T) {
	t.Parallel()
	f := newTestFlash(response.WithFlashMessageAsJS())
	if f.Message != "" {
		t.Error("Expected Message to be an empty string")
	}

	if f.MessageHTML != "" {
		t.Error("Expected MessageHTML to be an empty string")
	}

	if fmt.Sprintf("%T", f.MessageJS) != "template.JS" {
		t.Error("Expected message to be of type template.JS")
	}

	if f.MessageJS != "is a test message" {
		t.Error("Incorrect expected message")
	}
}

func TestWithFlashMessageAsString(t *testing.T) {
	t.Parallel()
	f := newTestFlash(response.WithFlashMessageAsString())
	if f.MessageJS != "" {
		t.Error("Expected Message to be an empty string")
	}

	if f.MessageHTML != "" {
		t.Error("Expected MessageHTML to be an empty string")
	}

	if fmt.Sprintf("%T", f.Message) != "string" {
		t.Error("Expected message to be of type template.JS")
	}

	if f.Message != "is a test message" {
		t.Error("Incorrect expected message")
	}
}
