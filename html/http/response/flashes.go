package response

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"html/template"
	"net/http"
	"time"
)

var (
	flashCtxKey = ctxKey("flash")

	// FlashQueryParam is the key used to check for a base64 encoded flash
	// query param from a request. Useful on http redirects where context is reset.
	FlashQueryParam = "flashes"
)

type (
	ctxKey string

	// FlashContentType is used to define the flash message content type
	FlashContentType uint8

	// Flashes is a slice of Flash instances
	Flashes []*Flash

	// Flash is the default struct for Flash messages
	Flash struct {
		FlashContentType
		FlashType
		Message     string
		MessageHTML template.HTML
		MessageJS   template.JS
		CreatedAt   int64
		Opts        map[string]interface{}
	}
	flashOptions struct {
		flashContentType FlashContentType
	}

	// FlashOption is used to assign options to flash messages
	FlashOption func(*flashOptions)

	// FlashType is used to denote the type of flash message, eg Success
	FlashType string
)

const (
	// FlashContentTypeString represents that the flash message should be rendered as plain text
	FlashContentTypeString FlashContentType = iota

	// FlashContentTypeHTML represents that the flash message should be rendered as template.HTML
	FlashContentTypeHTML

	// FlashContentTypeJS represents that the flash message should be rendered as template.JS
	FlashContentTypeJS

	// FlashAlert is a Flash Type for Alert messages
	FlashAlert FlashType = "alert"

	// FlashError is a Flash Type for Error messages
	FlashError FlashType = "error"

	// FlashInfo is a Flash Type for Info messages
	FlashInfo FlashType = "info"

	// FlashSuccess is a Flash Type for Success messages
	FlashSuccess FlashType = "success"
)

func (c ctxKey) String() string {
	return "response ctx key " + string(c)
}

// NewFlash will return the pointer to a new Flash instance
func NewFlash(ft FlashType, msg string, opts map[string]interface{}, flashOpts ...FlashOption) *Flash {
	f := &Flash{
		FlashType: ft,
		Opts:      opts,
		CreatedAt: time.Now().UnixNano(),
	}

	// Confirm the flash message content type to apply
	fopts := &flashOptions{}
	for _, flashOpt := range flashOpts {
		flashOpt(fopts)
	}

	f.FlashContentType = fopts.flashContentType
	switch fopts.flashContentType {
	case FlashContentTypeString:
		f.Message = msg
	case FlashContentTypeHTML:
		f.MessageHTML = template.HTML(msg)
	case FlashContentTypeJS:
		f.MessageJS = template.JS(msg)
	}

	return f
}

// AddFlash will find any existing flashes on the request context and append a new
func AddFlash(r *http.Request, ft FlashType, msg string, opts map[string]interface{}, flashOpts ...FlashOption) *http.Request {
	flashes := GetFlashes(r)
	if flashes == nil {
		flashes = Flashes{}
	}

	flashes = append(flashes, NewFlash(ft, msg, opts, flashOpts...))
	return r.WithContext(context.WithValue(r.Context(), flashCtxKey, flashes))
}

// AddFlashAlert is a convenience method to add an Alert flash type to the request context
func AddFlashAlert(r *http.Request, msg string, opts map[string]interface{}, flashOpts ...FlashOption) *http.Request {
	return AddFlash(r, FlashAlert, msg, opts, flashOpts...)
}

// AddFlashError is a convenience method to add an Error flash type to the request context.
func AddFlashError(r *http.Request, msg string, opts map[string]interface{}, flashOpts ...FlashOption) *http.Request {
	return AddFlash(r, FlashError, msg, opts, flashOpts...)
}

// AddFlashInfo is a convenience method to add an Info flash type to the request context.
func AddFlashInfo(r *http.Request, msg string, opts map[string]interface{}, flashOpts ...FlashOption) *http.Request {
	return AddFlash(r, FlashInfo, msg, opts, flashOpts...)
}

// AddFlashSuccess is a convenience method to add an Success flash type to the request context.
func AddFlashSuccess(r *http.Request, msg string, opts map[string]interface{}, flashOpts ...FlashOption) *http.Request {
	return AddFlash(r, FlashSuccess, msg, opts, flashOpts...)
}

// BindFlashesToRequestQuery will take the existing Flash entries, and bind to the requests URL.RawQuery, returning
// the RawQuery string for the caller. Used to set flash messages on redirect requests as Flash messages by default use
// request Context. Context is reset on redirects so this method allows for flash messages to be read from the query
func BindFlashesToRequestQuery(r *http.Request) string {
	f := GetFlashes(r)
	if f == nil {
		return ""
	}

	return f.BindToRequestQuery(r)
}

// First is a convenience method to return the first Flash from the Flashes is exists.
func (f Flashes) First() *Flash {
	if 0 < len(f) {
		return f[0]
	}
	return nil
}

// MarshalAndEncode is a convenience method to marshal Flashes into a base64 url encoded string.
func (f Flashes) MarshalAndEncode() string {
	b, err := json.Marshal(f)
	if err != nil {
		return ""
	}
	return base64.URLEncoding.EncodeToString(b)
}

// BindToRequestQuery will Marshal and Encode Flashes and "set" against the requests
// query values. A convenience method if you want your Flashes passed as a http query param.
func (f Flashes) BindToRequestQuery(r *http.Request) string {
	qs := f.MarshalAndEncode()
	vals := r.URL.Query()
	vals.Set(FlashQueryParam, qs)
	r.URL.RawQuery = vals.Encode()
	return r.URL.RawQuery
}

// GetFlashes will first check for a base64 encoded query param which can occur on http redirects, then from Context.
func GetFlashes(r *http.Request) Flashes {
	if msg := r.URL.Query().Get(FlashQueryParam); msg != "" {
		bytes, err := base64.URLEncoding.DecodeString(msg)
		if err == nil {
			f := Flashes{}
			err = json.Unmarshal(bytes, &f)
			if err == nil {
				return f
			}
		}
	}

	if f, ok := r.Context().Value(flashCtxKey).(Flashes); ok {
		return f
	}

	return nil
}

// HasFlashes will return true if any flashes are available on the request context.
func HasFlashes(r *http.Request) bool {
	if f := GetFlashes(r); f != nil {
		if 0 < len(f) {
			return true
		}
	}
	return false
}

// WithFlashMessageAsHTML option will ensure that the flash message is cast to a template.HTML type
func WithFlashMessageAsHTML() FlashOption {
	return func(opts *flashOptions) {
		opts.flashContentType = FlashContentTypeHTML
	}
}

// WithFlashMessageAsJS option will ensure that the flash message is cast to a template.JS type
func WithFlashMessageAsJS() FlashOption {
	return func(opts *flashOptions) {
		opts.flashContentType = FlashContentTypeJS
	}
}

// WithFlashMessageAsString option will ensure that the flash message output as a plain string
func WithFlashMessageAsString() FlashOption {
	return func(opts *flashOptions) {
		opts.flashContentType = FlashContentTypeString
	}
}
