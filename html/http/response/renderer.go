package response

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"

	"gitlab.com/kylehqcom/render"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
	"gitlab.com/kylehqcom/stencil/matcher"
)

type (

	// Renderer is the default interface to render templates for your web responses
	Renderer interface {
		// Compile will compile the bound loaders, matchers and executors. Note that the any loader
		// addition should enforce a recompile. If the Compile method has not been called explicitly,
		// the Render() method should call it implicitly.
		Compile() error

		// Render a response, using an http response & request matching a template name
		Render(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}, opts ...RenderOption) error

		// UseExecutor to bind a executor.Executor interface to this Renderer
		UseExecutor(executors ...executor.Executor)

		// UseLoader to bind a loader.Loader interface to this Renderer
		UseLoader(loaders ...loader.Loader)

		// UseMatcher to bind a matcher.Matcher interface to this Renderer
		UseMatcher(matchers ...matcher.Matcher)
	}

	// Executors is list of known/available executors.
	Executors []executor.Executor

	// Loaders is list of known/available loaders.
	Loaders []loader.Loader

	// Matchers is list of known/available matchers.
	Matchers []matcher.Matcher

	// RenderResponse is the struct used for the render http response templates
	RenderResponse struct {
		collection     *loader.Collection
		executors      Executors
		loaders        Loaders
		matchers       Matchers
		RenderOptions  *RenderOptions
		renderFuncName string
	}

	// RenderOption defines the behaviours for rendering
	RenderOption func(*RenderOptions)

	// RenderOptions are all availably defined behaviours for rendering
	RenderOptions struct {
		BaseTemplate   string
		Collection     *loader.Collection
		ErrorTemplate  string
		Executors      Executors
		FlashDataKey   string
		Loaders        Loaders
		Matchers       Matchers
		RenderFuncName string
		Reload         bool
	}
)

const DefaultRenderFuncName = "render"

var (
	// Ensure the Renderer implements the Renderer interface
	_ Renderer = &RenderResponse{}

	// templateWriter is used to render nested templates. Creating a package var to
	// resuse vs recreate for one less memalloc.
	templateWriter = bytes.NewBuffer(nil)
)

// DefaultTemplateFuncs will return the default template funcs expected to fulfil the Render
// Helpful for users of this package when wanting to merge template funcs of their own. Note
// that the default render func name can be changed, but not the current behaviour
func DefaultTemplateFuncs() template.FuncMap {
	return template.FuncMap{
		DefaultRenderFuncName: func() string {
			return ""
		},
	}
}

// NewRenderResponse will return a new RenderResponse pointer which implements the Renderer interface
func NewRenderResponse(opts ...RenderOption) *RenderResponse {
	rr := &RenderResponse{
		executors:      Executors{},
		loaders:        Loaders{},
		matchers:       Matchers{},
		RenderOptions:  NewRenderOptions(),
		renderFuncName: DefaultRenderFuncName,
	}

	for _, opt := range opts {
		opt(rr.RenderOptions)
	}
	rr.executors = rr.RenderOptions.Executors
	rr.loaders = rr.RenderOptions.Loaders
	rr.matchers = rr.RenderOptions.Matchers
	return rr
}

// Compile will compile the bound loaders, matchers and executors.
func (rr *RenderResponse) Compile() error {
	if 0 == len(rr.executors) {
		return render.ErrEmptyExecutors
	}

	if 0 == len(rr.loaders) {
		return render.ErrEmptyLoaders
	}

	if 0 == len(rr.matchers) {
		return render.ErrEmptyMatchers
	}

	rr.collection = loader.Collection{}.New()
	for _, loader := range rr.loaders {
		col, err := loader.Load()
		if err != nil {
			return err
		}
		// Iterate over every template and clone to the root collection
		// Note: do not AddParseTree() as this will NOT bind template funcs
		for _, t := range col.T.Templates() {
			rr.collection.T, err = t.Clone()
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Render attempts to match a template by string name and render via render options against an http response
// Note the current behaviours in processing
//  - If loader.Collection is nil, Compile() will be called which will load all loaders, but return on first error
//  - Each executor will be called when executing but return on first error
//  - The first match will be used when matching
// These behaviours may change in future as needs requirements grow. Eg a consistent response communicated via channels
func (rr *RenderResponse) Render(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}, opts ...RenderOption) error {

	// Compile on nil template collection
	if rr.collection == nil {
		if err := rr.Compile(); err != nil {
			return err
		}
	}

	var (
		baseTemplate    *template.Template
		contentTemplate *template.Template
		errorTemplate   *template.Template
	)

	// Passed in RenderOptions take precedence but we respect previous
	// default values by copying the values, not the pointer reference.
	options := *rr.RenderOptions
	for _, opt := range opts {
		opt(&options)
	}

	// On any render errors call this return func to render the error
	errorReturn := func(templateName string, templateError error) error {
		if errorTemplate != nil {
			var executeErr error
			for _, executor := range rr.executors {
				err := executor.Execute(w, errorTemplate, map[string]error{"error": templateError})
				if err != nil {
					executeErr = err
					break
				}
			}

			if executeErr == nil {
				return nil
			}

			// We have an error on rendering the actual default error template so
			// assign the vars to render an error string instead.
			templateError = executeErr
			templateName = errorTemplate.Name()
		}
		fmt.Fprintf(w, "Unexpected error. Template Name: %s Error: %s", templateName, templateError.Error())
		return templateError
	}

	// Locate the default Error template if defined
	if options.ErrorTemplate != "" {
		var errTplMatchErr error
		for _, matcher := range rr.matchers {
			errTpl, err := matcher.Match(rr.collection, options.ErrorTemplate)
			if errTpl != nil {
				errorTemplate = errTpl
				errTplMatchErr = nil
				break
			}
			errTplMatchErr = err
		}
		if errTplMatchErr != nil {
			return errorReturn(options.ErrorTemplate, errTplMatchErr)
		}
	}

	// Locate the default base template if defined
	if options.BaseTemplate != "" {
		var baseMatchErr error
		for _, matcher := range rr.matchers {
			baseTpl, err := matcher.Match(rr.collection, options.BaseTemplate)
			if baseTpl != nil {
				baseTemplate = baseTpl
				baseMatchErr = nil
				break
			}
			baseMatchErr = err
		}
		if baseMatchErr != nil {
			return errorReturn(options.BaseTemplate, baseMatchErr)
		}
	}

	// Now locate the template to Render by name
	var matchErr error
	var matchTpl *template.Template
	for _, matcher := range rr.matchers {
		t, err := matcher.Match(rr.collection, name)
		if t != nil {
			matchTpl = t
			matchErr = nil
			break
		}
		matchErr = err
	}
	if matchErr != nil {
		return errorReturn(name, matchErr)
	}

	if data == nil {
		data = make(map[string]interface{})
	}

	// Add any flash messages from the request
	flashKey := FlashQueryParam
	if "" != options.FlashDataKey {
		flashKey = options.FlashDataKey
	}
	data[flashKey] = GetFlashes(r)

	var err error
	if baseTemplate != nil {
		baseTemplate.Funcs(template.FuncMap{
			rr.renderFuncName: func() (template.HTML, error) {
				templateWriter.Reset()
				var executeErr error
				for _, executor := range rr.executors {
					err = executor.Execute(templateWriter, matchTpl, data)
					if err != nil {
						executeErr = err
						break
					}
				}
				return template.HTML(templateWriter.String()), executeErr
			},
		})
		contentTemplate = baseTemplate
	} else {
		contentTemplate = matchTpl
	}

	// Set default headers
	w.Header().Set("Content-Type", "text/html")

	// Execute the template
	var executeErr error
	for _, executor := range rr.executors {
		err := executor.Execute(w, contentTemplate, data)
		if err != nil {
			executeErr = err
			break
		}
	}

	if executeErr != nil {
		return errorReturn(contentTemplate.Name(), executeErr)
	}

	return nil
}

// UseExecutor to bind a executor.Executor interface to this Renderer
func (rr *RenderResponse) UseExecutor(executors ...executor.Executor) {
	rr.executors = append(rr.executors, executors...)
}

// UseLoader to bind a loader.Loader interface to this Renderer
func (rr *RenderResponse) UseLoader(loaders ...loader.Loader) {
	rr.loaders = append(rr.loaders, loaders...)
	rr.collection = nil
}

// UseMatcher to bind a matcher.Matcher interface to this Renderer
func (rr *RenderResponse) UseMatcher(matchers ...matcher.Matcher) {
	rr.matchers = append(rr.matchers, matchers...)
}

// NewRenderOptions will return a new RenderOptions pointer
func NewRenderOptions() *RenderOptions {
	return &RenderOptions{
		FlashDataKey: FlashQueryParam,
	}
}

// RenderWithBaseTemplate will define the template that all other template matches will inherit or embed from
func RenderWithBaseTemplate(name string) RenderOption {
	return func(opts *RenderOptions) {
		opts.BaseTemplate = name
	}
}

// RenderWithErrorTemplate will use the name provided to match if any errors are returned on render
func RenderWithErrorTemplate(name string) RenderOption {
	return func(opts *RenderOptions) {
		opts.ErrorTemplate = name
	}
}

// RenderWithExecutor will bind executors on new RenderResponse Renderer
func RenderWithExecutor(executors ...executor.Executor) RenderOption {
	return func(opts *RenderOptions) {
		opts.Executors = append(opts.Executors, executors...)
	}
}

// RenderWithFlashDataKey will assign your preferred string key name for flash messages bound to the template data
func RenderWithFlashDataKey(key string) RenderOption {
	return func(opts *RenderOptions) {
		opts.FlashDataKey = key
	}
}

// RenderWithRenderFuncName will set the render fucn name to render the contents of a template.
// This defaults to "render", or as inside your template {{render}}
func RenderWithRenderFuncName(name string) RenderOption {
	return func(opts *RenderOptions) {
		opts.RenderFuncName = name
	}
}

// RenderWithLoader will bind loaders on new RenderResponse Renderer
func RenderWithLoader(loaders ...loader.Loader) RenderOption {
	return func(opts *RenderOptions) {
		opts.Loaders = append(opts.Loaders, loaders...)
	}
}

// RenderWithMatcher will bind matchers on new RenderResponse Renderer
func RenderWithMatcher(matchers ...matcher.Matcher) RenderOption {
	return func(opts *RenderOptions) {
		opts.Matchers = append(opts.Matchers, matchers...)
	}
}
