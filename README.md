```
<>================================<>
||\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/||
||<> <> <> <> <> <> <> <> <> <> <>||
||/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\||                        RENDER
||================================||
||<> <> <> <> <> <> <> <> <> <> <>||     A package to Render your Go Stenciled Templates
||================================||
||/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\||
||<> <> <> <> <> <> <> <> <> <> <>||
||\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/||
<>================================<>
```

# Render - A package to render your Go templates

Note that *Render* leverages the [https://gitlab.com/kylehqcom/stencil](https://gitlab.com/kylehqcom/stencil) package to provide template rendering via a web facing context. As templating in Go is not always trivial, this package has been designed to take a lot of the _"hurt"_ out of loading,retrieving and rendering templates and nested templates by using smart name matching, with locales and sensible fallbacks.

If you like this package or are using for your own needs, then let me know via [https://twitter.com/kylehqcom](https://twitter.com/kylehqcom)

## Features

- Well defined simple interfaces
- Easily extensible to suit your needs
- Fully tested
- Handles i18n/locales with fallback
- Cascading template match rules
- Render default Error pages
- Flash messaging already built in

## Installation

```
// Install with the usual or add to you package manager of choice.
go get -u gitlab.com/kylehqcom/render
```

## Quickstart example

If you are wanting to dive straight in, you can take a look at the examples folder found here [https://gitlab.com/kylehqcom/render/examples](https://gitlab.com/kylehqcom/render/examples). Examples use this packages _"Renderer"_ interface to render your responses. This (in my opinion) will cover 95% of your use cases. Note that in order to render the index handler example, we assume that you have a folder structure of

```text
your/dir/to/templates/
  error.html
  layout.html
  en/
    index.html
  es/
    index.html
```

```go
package main

import (
    "html/template"
    "log"
    "net/http"
    "path/filepath"

    "gitlab.com/kylehqcom/render/response"
    "gitlab.com/kylehqcom/stencil/executor"
    "gitlab.com/kylehqcom/stencil/loader"
    "gitlab.com/kylehqcom/stencil/matcher"
)

func main() {
    setupTemplates()
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        // Note we only need to refer to the filename to lookup as the PathRoot option is defined in setupTemplates
        r = response.AddFlashInfo(r, "route is index", nil)
        render(w, r, "index.html", nil)
    })
    log.Println("Enjoy using Render")
    log.Println("Example now running on http://localhost:80")
    log.Fatal(http.ListenAndServe("", nil))
}

// render is responsible for rendering the html pages
// Normally you would this to your http handler for easy reference, not a package level var
func (h *handler) render(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}, opts ...response.RenderOption) {
    if data == nil {
        data = make(map[string]interface{})
    }
    data["csrf"] = h.sessionManager.GetString(r.Context(), someKeyEtc)
    h.responseRenderer.Render(w, r, name, data, opts...)
}


// Normally you would this to your http handler for easy reference, not a package level var
func (h *handler) setupTemplates() {
    tplDir := "src/www/templates"
    fileloader := loader.NewFilepathLoader([]string{
        filepath.Join(tplDir, "/*.html"),
        filepath.Join(tplDir, "/*/*.html"),
    }, loader.WithMustParse(true))

    // This example has a yet to be loaded template with a template.func of {{render}}, so we placeholder to ensure parsing
    fileloader.RegisterFuncs(template.FuncMap{
        "render": func() string {
            return "Render called unexpectedly"
    }})

    responseRenderer = response.NewRenderResponse(response.RenderWithBaseTemplate("index.html"))
    responseRenderer.UseExecutor(executor.NewTemplateExecutor())
    responseRenderer.UseLoader(fileloader)
    responseRenderer.UseMatcher(matcher.NewFilepathMatcher(
        matcher.WithPathRoot(tplDir),
    ))
    h.responseRenderer = responseRenderer
}
```

### Flash messages, bonus extras

As **Render** package also takes care of your `*http.Request` to `*http.Request` *flash* messages.

There are x4 Flash types bundled with **Render**

```go
const (
  // FlashAlert is a Flash Type for Alert messages
  FlashAlert FlashType = "alert"

  // FlashError is a Flash Type for Error messages
  FlashError FlashType = "error"

  // FlashInfo is a Flash Type for Info messages
  FlashInfo FlashType = "info"

  // FlashSuccess is a Flash Type for Success messages
  FlashSuccess FlashType = "success"
)
```

But of course you can cast your own `FlashType` from a string source. Adding a flash message is as easy as calling
the `Add{FlashType}()` or `AddFlash()` methods respectively. Example

```go
// Both are equivalent
r = render.AddFlashSuccess(r *http.Request, "a success message", nil)
r = render.AddFlash(render.FlashSuccess, r *http.Request, "a success message", nil)
```

You can even add flash messages **from outside the Go context** by calling the `BindToRequestQuery` method on your `Flashes` instance. The `BindFlashesToRequestQuery` which takes and returns the `request.URL.RawQuery` string, will base64 encode flashes and add as a query param to your request. By default the query param key of `flashes` is used but you can optionally change that too. This is really handy when redirecting users but wanting to display a Flash message on redirect completion. Example

```go
r := *http.Request
// Flashes are now bound on the url.Values of the *http.request as a Base64 Encoded string. As a
// convenience the request URL.RawQuery value is returned for the redirect
rawQueryString := render.BindFlashesToRequestQuery(r)

// On redirect, use the r.URL.RawQuery or the string returned from the call above
http.Redirect(w, r, fmt.Sprintf("/you/request/uri?%s", rawQueryString), http.StatusFound)
```

If you are using a `RenderResponse` instance, then everything is taken care of. The renderer will add flash messages to your template `data map[string]interface` under the key of `flashes` by first checking for flash messages on the request query, and then on the requests' context. Note that the key of `flashes` is configurable and you can retrieve the Base64 encoded value and type assert as required. Refer to the `GetFlashes` func of [https://gitlab.com/kylehqcom/render/blob/main/flashes.go](https://gitlab.com/kylehqcom/render/blob/main/flashes.go) for an example.

One last thing around flashes. There are also `FlashContentType` options you can apply. For example, there are times when displaying a string message isn't desirable. Sometimes you may wish to add HTML to your flash message, eg to give a user an href/link. Golang by default escapes all rendered template text so you need to pass either the `WithFlashMessageAsHTML` or the `WithFlashMessageAsJS` flash options when adding/creating. The `WithFlashMessageAsString` is the default option but is available if you need to revert. There are security considerations to be aware of (refer [https://golang.org/src/html/template/content.go](https://golang.org/src/html/template/content.go)), but as long as you trust your own source files, you will be fine.

```go
// Example with flash content type example
r := *http.Request
r = render.AddFlashAlert(r,`<a href="#">Flash link</a>`, nil, render.WithFlashMessageAsHTML())
```
