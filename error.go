package render

// Error is the Render package error type
type Error struct {
	s string
}

var (
	// ErrEmptyExecutors is returned when executors is an empty slice
	ErrEmptyExecutors = NewRenderError("Empty executors")

	// ErrEmptyLoaders is returned when loaders are an empty slice
	ErrEmptyLoaders = NewRenderError("Empty loaders")

	// ErrEmptyMatchers is returned when matchers are an empty slice
	ErrEmptyMatchers = NewRenderError("Empty matchers")
)

// Error will return the string representation of a RenderError
func (e Error) Error() string {
	return e.s
}

// IsStencilError will return true on Error Stencil type
func IsRenderError(err error) bool {
	_, ok := err.(*Error)
	return ok
}

// NewRenderError will create a new Error Render type error
func NewRenderError(errorMessage string) error {
	return &Error{errorMessage}
}
